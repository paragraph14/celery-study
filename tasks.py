# -*- coding: utf-8 -*-

# http://yuku-tech.hatenablog.com/entry/20101018/1287391988
# http://qiita.com/_rdtr/items/d3bc1a8d4b7eb375c368

from celery.task import Task
# Task クラスをインポート
from cdlery.decorators import task

class AddTask(Task):
    def run(self, x, y):
        logger = self.get_logger(task_name='クラス')
        # 親クラスである Task のメソッドをって logger オブジェクトを生成
        logger.info('Adding {} + {}'.format(x, y))
        return x + y
    # 引数の和を返す

@task
# これはデコレータ
def add(x,y):
    logger = Task.get_logger(task_name='デコレータ')
    logger.info('Adding {} + {}'.format(x, y))
    return x + y

# デコレータが宣言されているので
# add = task(add)
